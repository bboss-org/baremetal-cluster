# Cluster Powersupply

## Technical specifications

|**Device**     |**Input voltage**|**Input Current**|**Output voltage**|**Output Current**|
|---------------|:---------------:|:---------------:|:----------------:|:----------------:|
|Dlink DGS-1005D|  100-240 V      |  0.2 A          |   5 V            |  1.20 A          |
|Intel NUC7i3BNH|  100-240 V      |  1.5 A          |  19 V            |  3.43 A          |
|Intel NUC7i5BNH|  100-240 V      |  1.5 A          |  19 V            |  3.43 A          |
|Intel NUC7i5BNH|  100-240 V      |  1.5 A          |  19 V            |  3.43 A          |
|Intel NUC7i7BNH|  100-240 V      |  1.5 A          |  19 V            |  3.43 A          |
|**Total**      |**100-240 V**    |**6.2 A**        |                  |                  |

## Intel NUC powersupply customization

The Intel NUC powersupply need a small customization to fit into the designed layout.

To maintain electric safety and preserve product's warranty, the customization is made on the dust cover provided with the powersupply.

## Assembly component list
|**Quantity**|**Reference**|**Description**                                         |**Links**|
|:----------:|-------------|--------------------------------------------------------|---------|
| 1 meter    |HO7VU 1.5    |PVC Insulated electric wire with solid copper conductor<br>(1.5mm thickness)|[Amazon](https://www.amazon.fr/dp/product/B072DXF42J)  
| 1          |LEG98431     |Plastic Electrical Screw Terminal Block (6mm capability)|[Amazon](https://www.amazon.fr/dp/product/B00HCLMMNI)
| 4          |M2x15        |M2 plastic/nylon bolt (electrical terminal fastening)   |[Amazon](https://www.amazon.fr/gp/product/B07CJGT93C)
| 4          |M2 Nut       |M2 plastic/nylon nut (electrical terminal fastening)    |[Amazon](https://www.amazon.fr/gp/product/B07CJGT93C)

## Assembly instructions

### 1. Internal connectors
**With the HO7VU copper wire, create 8 internal connectors, 2 per powersupply :**

<img src="InternalWire/internal_wire_01.jpg" width=400>
<img src="InternalWire/internal_wire_02.jpg" width=400>
<img src="InternalWire/internal_wire_03.jpg" width=400>

### 2. Dust cover
**Remove the dust cover from the powersuplly :**

<img src="DustCover/dust_cover_01.jpg" width=400>
<img src="DustCover/dust_cover_02.jpg" width=400>
<img src="DustCover/dust_cover_03.jpg" width=400>
<img src="DustCover/dust_cover_04.jpg" width=400>

**With a 2.5 mm bit, drill 2 holes for internal connectors :**

<img src="DustCover/dust_cover_05.jpg" width=400>
<img src="DustCover/dust_cover_06.jpg" width=400>

**and  1 hole for the M2 nylon bolt :**

<img src="DustCover/dust_cover_07.jpg" width=400>
<img src="DustCover/dust_cover_08.jpg" width=400>

**Replace the 2.5 mm bit with a 3 mm bit :**
- **With the 3 mm bit, enlarge connectors holes.**
- **Drill the external partition only, do not modify the internal separator !!!**

<img src="DustCover/dust_cover_09.jpg" width=400>
<img src="DustCover/dust_cover_10.jpg" width=400>

**Assemble the electrical terminal block on the cover :**
- **Cut LEG98431 to make 1 terminal with 2 connectors.**
- **Insert M2x15 into the cover's bolt hole, the head must fit inside the cover's square cell.**
- **Insert M2x15 into the terminal block's hole.** 
- **Fix the terminal block with the nylon nut.**

<img src="DustCover/dust_cover_11.jpg" width=400>
<img src="DustCover/dust_cover_12.jpg" width=400>

**Insert internal connectors :**
- **Shortest branch inside the terminal block.** 
- **Longest branch inside the cover.**

<img src="DustCover/dust_cover_13.jpg" width=400>
<img src="DustCover/dust_cover_14.jpg" width=400>

**Check internal connector positions and tighten terminal screws :**

<img src="DustCover/dust_cover_15.jpg" width=400>
<img src="DustCover/dust_cover_16.jpg" width=400>

**Put the cover on the powersupply and gently slide it until it clicks :**

<img src="DustCover/dust_cover_17.jpg" width=400>
<img src="DustCover/dust_cover_18.jpg" width=400>

**With the HO7VU copper wire, create 6 external connectors and connect all powersupplies together :**

<img src="ExternalWire/external_wire_01.jpg" width=400>
<img src="ExternalWire/external_wire_02.jpg" width=400>
<img src="ExternalWire/external_wire_03.jpg" width=400>
<img src="ExternalWire/external_wire_04.jpg" width=400>
