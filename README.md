# Portable Baremetal Cluster

<img width=800 src="Drawings/Shelves.png">

## Portable cluster requirements
- Components must be securely fixed on the shelves.
- Design must be solid enougth for frequent moves.
- Design must be large enought to allow "passive cluster" ventillation.
- Design must be as small as possible for an easy transportation.
- External network connection must use one cable.
- External power connection must use one power cord.
- Devices or powersupplies must not be modified to preserve product's warranty.

## Computing resources
|**Device**|**Cores**|**Threads**|**Frequency**   |**Memory**|**Storage** |
|----------|--------:|----------:|---------------:|---------:|-----------:|
|NUC7i3BNH |  2      |   4       |  2.40 GHz      |   16 Go  |    500 Go  |
|NUC7i5BNH |  2      |   4       |  2.20-3.40 GHz |   32 Go  |    500 Go  |
|NUC7i5BNH |  2      |   4       |  2.20-3.40 GHz |   32 Go  |    500 Go  |
|NUC7i7BNH |  2      |   4       |  3.50-4.00 GHz |   32 Go  |    500 Go  |
|**Total** |**8**    |**16**     |                |**112 Go**|**2 000 Go**|

## Support materials
|**Files**                 |**Description**                                                             |
|--------------------------|----------------------------------------------------------------------------|
|`./Drawings/*`            | Files and specifications used to create the Freecad project.               |
|`./Drawings/Shelves.FCStd`| [Freecad v0.17](https://www.freecadweb.org) project.                       |
|`./LaserCut/*`            | Files used by LaserWeb to cut shelves.                                     |
|`./LaserCut/*.json`       | [LaserWeb v4.0.991](https://github.com/LaserWeb/LaserWeb4/wiki) workspaces.|
|`./PowerSupply/*`         | Powersupply customization guide.                                           |

**Notes :**
- Shelves are numbered from the bottom to the top.
- Shelf one is the lowest.
- In Freecad, select sketches instead than the final objet before calling export.
- When a 3D object is exported, SVG or DXF format is not recognized by Laserweb.

To adapt the top shelf to your ethernet router, in the Freecad project :
- Edit `Sketches/skPlate_router_holes`
- Make modifications matching your router specifications.
- Select both :
  - `Sketches/skPlate`
  - `Sketches/skPlate_router_holes`
- Export to Flattened SVG : `./LaserCut/Shelf 04.svg`
- Update corresponding LaserWeb workspace.

## MDF Shelves kit component list
|**Quantity**|**Reference**  |**Description**                         |**Links**|
|:----------:|---------------|----------------------------------------|---------|
| 1          |MDF3.2         |Medium density fiber 3.2 x 800 x 400    |[Leroy-Merlin](https://www.leroymerlin.fr/v3/p/produits/predecoupe-fibres-dures-blanc-ep-3-2-mm-l-80-x-l-40-cm-e154680)
| 6          |M5x12          |Top shelf fastening bolts               |[Amazon](https://www.amazon.fr/gp/product/B075GWGGP8)
| 6          |TFF-M5X70/DR128|Screwed spacer sleeve Int.thread M5 70mm|[Amazon](https://www.amazon.fr/gp/product/B00Z7CJ9AS)<br>[Specifications](https://www.tme.eu/en/details/tff-m5x70_dr128/metal-spacers/dremec/128x70)
|12          |TFM-M5X70/DR228|Screwed spacer sleeve Int.thread M5 70mm|[Amazon](https://www.amazon.fr/gp/product/B00YHJIQRY)<br>[Specifications](https://www.tme.eu/en/details/tfm-m5x70_dr228/metal-spacers/dremec/228x70) 
| 6          |M12x16x10mm    |Rubber foot                             |[Amazon](https://www.amazon.fr/dp/B07B2TK86D)
| 6          |M5x16          |Rubber foot fastening bolts             |[Amazon](https://www.amazon.fr/gp/product/B075GWGGP8)

## PMMA Shelves kit component list
<img width=400 src="Drawings/Shelves_PMMA_01.jpg">
<img width=400 src="Drawings/Shelves_PMMA_02.jpg">

The PPMA Shelves kit has some cracking issues around bolt holes, I guess it is due to a cheap and low quality PMMA (Extruded) from Amazon.<br>
I'm working on solutions with drawn PPMA (Plexiglass, Altuglass) and rubber rings.<br>
I will update this document as soon as right products are identified.

|**Quantity**|**Reference**  |**Description**                         |**Links**|
|:----------:|---------------|----------------------------------------|---------|
| 2          |PMMA M4        |Plexiglass plate (shelf) 400x400x4mm    |
| 6          |M5x12          |Top shelf fastening bolts               |[Amazon](https://www.amazon.fr/gp/product/B075GWGGP8)
| 6          |TFF-M5X70/DR128|Screwed spacer sleeve Int.thread M5 70mm|[Amazon](https://www.amazon.fr/gp/product/B00Z7CJ9AS)<br>[Specifications](https://www.tme.eu/en/details/tff-m5x70_dr128/metal-spacers/dremec/128x70)
|12          |TFM-M5X70/DR228|Screwed spacer sleeve Int.thread M5 70mm|[Amazon](https://www.amazon.fr/gp/product/B00YHJIQRY)<br>[Specifications](https://www.tme.eu/en/details/tfm-m5x70_dr228/metal-spacers/dremec/228x70) 
| 6          |M12x16x10mm    |Rubber foot                             |[Amazon](https://www.amazon.fr/dp/B07B2TK86D)
| 6          |M5x16          |Rubber foot fastening bolts             |[Amazon](https://www.amazon.fr/gp/product/B075GWGGP8)

## Accessory component list
|**Quantity**|**Reference**|**Description**                            |**Links**|
|:----------:|-------------|-------------------------------------------|---------|
|8           |M3X12        |Intel NUC fastening bolts                  |[Amazon](https://www.amazon.fr/gp/product/B075GWGGP8)
|1           |100x2.5      |Pack of black cable ties 100mm x 2.5mm     |[Amazon](https://www.amazon.fr/gp/product/B004HSMU0Y)
|1           |150x2.5      |Pack of black cable ties 150mm x 2.5mm     |[Amazon](https://www.amazon.fr/gp/product/B07B2C4TJ8)
|1           |200x2.5      |Pack of black cable ties 200mm x 2.5mm     |[Amazon](https://www.amazon.fr/gp/product/B075GWGGP8)
|4           |CAT6 0.25m   |Ethernet 0.25m (metres) CAT 6 network cable|[Amazon](https://www.amazon.fr/gp/product/B06WP1KMKP)
|1           |PWCORD       |Power cord supporting at least 7A          |